use std::{net::{ToSocketAddrs, TcpStream, UdpSocket, IpAddr, Ipv4Addr, Ipv6Addr, SocketAddr}, cell::RefCell, time::Duration, mem::MaybeUninit, ops::RangeInclusive};

use crate::{Result, Error, proto::{Ctrl, Telegram, Request, ModBus, PayloadExtra, misc::Registers, be16, TelegramBuilder}, device::{DeviceInfo, Stats}};

pub struct Client<'a> {
    slave_id:	u8,
    server:	String,
    builder:	RefCell<Option<TelegramBuilder>>,
    id:		RefCell<u32>,
    is_offline:	RefCell<bool>,
    device:	&'a DeviceInfo<'a>,

    verbose:	bool,
    dump_regs:	bool,
}

impl <'a> Client <'a> {
    const DISCOVER_PORT: u16 = 48899;
    const PROTO_PORT: u16 = 8899;
    const DISCOVER_REQUEST: &'static [u8] = b"WIFIKIT-214028-READ";

    const CONNECT_TIMEOUT: Duration = Duration::from_millis(2_000);
    const READ_TIMEOUT: Duration = Duration::from_millis(15_000);
    const MAX_RETRIES_STATUS: u32 = 3;

    pub fn new(server: &str, device: &'a DeviceInfo<'a>) -> Self {
	Self {
	    device:	device,
	    slave_id:	1,
	    server:	server.to_owned(),
	    builder:	RefCell::new(None),
	    id:		RefCell::new(0),
	    is_offline:	RefCell::new(false),
	    verbose:	false,
	    dump_regs:	true,
	}
    }

    pub fn set_dump_regs(&mut self, do_dump: bool) {
	self.dump_regs = do_dump;
    }

    fn discover_one(&self, addr: SocketAddr) -> Result<()> {
	use crate::io::ReadUninit;

	let local: IpAddr = match addr.ip() {
	    i if i.is_ipv4()	=> Ipv4Addr::UNSPECIFIED.into(),
	    i if i.is_ipv6()	=> Ipv6Addr::UNSPECIFIED.into(),
	    _			=> panic!("unsupported address family {addr}"),
	};

	let socket = UdpSocket::bind(SocketAddr::new(local, 0))?;

	socket.set_read_timeout(Some(Duration::from_millis(2000)))?;

	socket.connect(addr)?;
	socket.send(Self::DISCOVER_REQUEST)?;

	let mut buf = [MaybeUninit::<u8>::uninit();1024];
	let buf = socket.read_uninit(&mut buf)?;

	let resp = crate::proto::WifiKitResponse::parse(buf)
	    .ok_or(Error::BadDiscoveryResponse)?;

	info!("discovered device {resp:?}");

	let builder = TelegramBuilder::new(resp.serial);

	self.builder.replace(Some(builder));
	self.id.replace(resp.serial);

	Ok(())
    }

    fn discover(&self) -> Result<()> {
	if self.builder.borrow().is_some() {
	    return Ok(());
	}

	for mut addr in self.server.to_socket_addrs()? {
	    addr.set_port(Self::DISCOVER_PORT);

	    let _ = self.discover_one(addr);

	    if self.builder.borrow().is_some() {
		break;
	    }
	}

	Ok(())
    }

    fn is_response(data: &Telegram) -> bool {
	if data.header.get_ctrl() != Ctrl::INFO_RESPONSE {
	    return false;
	}

	let payload = data.get_payload();

	// check whether:
	// - functional code is 3
	// - payload is evenly aligned for 16 bit registers (protocol adds 5 bytes)
	payload.len() > 3 && payload[1] == 3 && payload.len() % 2 == 1
    }

    fn fetch_registers(&self, conn: &mut TcpStream, range: RangeInclusive<u16>)
		       -> Result<(PayloadExtra, Vec<u8>)> {
	debug!("requesting registers {range:?}");

	let builder = self.builder.borrow();
	let builder = builder.as_ref().unwrap();

	let m_request = ModBus::ReadHoldingRegisters(range).generate(self.slave_id);
	let request = Request::SendDataField(&m_request).generate();

	let data = builder.create_telegram(Ctrl::INFO, &request);

	data.send(conn)?;

	let mut buf = crate::io::uninit_buf::<1024>();

	let data = loop {
	    match Telegram::recv(conn, &mut buf) {
		Ok(data) if Self::is_response(&data)	=> break data,
		Ok(data)				=> info!("got unexpected data {data:?}"),
		Err(e)		=> {
		    warn!("failed to receive response: {e:?}");
		    return Err(e.into());
		}
	    }
	};

	let extra = data.get_payload_extra();
	let extra = PayloadExtra::from_raw(extra)
	    .ok_or(Error::BadPayloadExtra)?;

	if self.verbose {
	    debug!("received data  {data:x?}");
	    debug!("received extra {extra:?}");
	}

	Ok((extra, data.get_payload()[3..].to_owned()))
    }

    fn request_info(&self, conn: &mut TcpStream) -> Result<Stats> {
	let specs = self.device.specs;
	let mut reg_range = crate::device::Spec::get_range(specs);
	let reg_start = *reg_range.start();
	let reg_end   = *reg_range.end();

	let mut reg_data = Vec::with_capacity((reg_end - reg_start) as usize + 1);
	let mut proto_extra = None;

	while reg_end >= *reg_range.start() {
	    let start = *reg_range.start();
	    let count = (reg_end - start + 1).min(0x7f);

	    let (e, data) = self.fetch_registers(conn, start..=(start + count - 1))?;

	    reg_data.extend_from_slice(&data);
	    proto_extra = Some(e);

	    reg_range = (start + count)..=reg_end;
	}

	let registers = Registers::<be16>::from_slice(reg_start, &reg_data)
	    .ok_or(Error::BadDiscoveryResponse)?;

	if self.dump_regs {
	    if let Some(e) = &proto_extra {
		info!("{:50}\t{:?} ({} + {})", "Time", e.total_tm(), e.ref_tm(), e.rel_tm());
		info!("{:50}\t{} s", "Uptime", e.uptime().as_secs());
	    }

	    for s in specs {
		info!("{:50}\t{}", s.name, s.formatter(&registers));
	    }
	}

	let mut stats = self.device.as_stats(&registers)
	    .ok_or(Error::BadDiscoveryResponse)?;

	if let Some(e) = proto_extra {
	    stats.fill_extra_info(&e);
	}

	stats.set_id(*self.id.borrow());

	if stats.status != 4 {
	    debug!("unexpected registers {registers:?}");
	}

	Ok(stats)
    }

    pub fn is_offline(&self) -> bool {
	*self.is_offline.borrow()
    }

    pub fn run(&self) -> Result<Stats> {
	let mut conn = None;
	let mut last_err = None;

	self.discover()?;

	for mut addr in self.server.to_socket_addrs()? {
	    if addr.port() == 0 {
		addr.set_port(Self::PROTO_PORT);
	    }

	    match TcpStream::connect_timeout(&addr, Self::CONNECT_TIMEOUT) {
		Ok(c)		=> {
		    conn = Some(c);
		    break;
		},

		Err(e)		=> {
		    debug!("failed to connect to {addr:?}: {e:?}");
		    last_err = Some(e);
		    continue;
		}
	    }
	}

	let mut conn = match (conn, last_err) {
	    (None, Some(e))	=> {
		if !self.is_offline.replace(true) {
		    error!("failed to connect to {}: {e:?}", self.server);
		} else {
		    debug!("failed to connect to {}: {e:?}", self.server);
		}

		Err(e.into())
	    }

	    (None, None)	=> {
		error!("failed to resolve {}", self.server);

		Err(crate::Error::NoSuchHost)
	    }

	    (Some(c), _)	=> {
		self.is_offline.replace(false);

		Ok(c)
	    }
	}?;

	conn.set_read_timeout(Some(Self::READ_TIMEOUT))?;

	let mut retries = Self::MAX_RETRIES_STATUS;

	loop {
	    match self.request_info(&mut conn) {
		Err(e) if retries > 0	=> {
		    warn!("error {e:?} while requesting info; retrying...");
		    retries -= 1;
		},
		res			=> break res,
	    }
	}
    }
}
