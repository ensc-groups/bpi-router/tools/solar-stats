use chrono::Timelike;
use rusqlite::Connection;

use crate::device::Stats;

use super::DbStats;

impl DbStats for Stats {
    fn create_db(&self, conn: &mut Connection) -> rusqlite::Result<()> {
	conn.execute("CREATE TABLE IF NOT EXISTS daily_production (
date	DATE NOT NULL UNIQUE,
updated DATETIME NOT NULL,
total	FLOAT NOT NULL,
pv1	FLOAT NOT NULL,
pv2	FLOAT NOT NULL
)", [])?;

	Ok(())
    }

    fn write_values(&self, conn: &mut Connection) -> rusqlite::Result<()> {
	use chrono::Datelike;
	use rusqlite::params;

	let tm = chrono::DateTime::<chrono::Local>::from(self.tm).naive_utc();

	conn.execute("INSERT OR REPLACE INTO daily_production(date, updated, total, pv1, pv2)
VALUES (?1, ?2, ?3, ?4, ?5)",
		     params![format!("{:04}-{:02}-{:02}", tm.year(), tm.month(), tm.day()),
			     format!("{:04}-{:02}-{:02} {:02}:{:02}:{:02}",
				     tm.year(), tm.month(), tm.day(),
				     tm.hour(), tm.minute(), tm.second()),
			     self.prod_daily_all,
			     self.prod_daily[0],
			     self.prod_daily[1]])?;

	Ok(())
    }
}

#[cfg(test)]
mod test {
    use crate::{device::Stats, db::DbStats};

    #[test]
    pub fn test_00() {
	let mut conn = rusqlite::Connection::open_in_memory().unwrap();
	let stats = Stats {
	    id:			"test".to_string(),
	    tm:			crate::Time::now(),
	    uptime:		Default::default(),
	    run_time:		Default::default(),
	    num_pv:		Default::default(),
	    status:		Default::default(),
	    prod_daily_all:	23.,
	    prod_daily:		[12., 11., 0., 0.],
	    prod_total_all:	Default::default(),
	    prod_total:		Default::default(),
	    ac_voltage:		Default::default(),
	    ac_frequency:	Default::default(),
	    ac_output_power:	Default::default(),
	    temperature:	Default::default(),
	    voltage:		Default::default(),
	    current:		Default::default(),
	};

	conn.execute_batch("PRAGMA journal_mode = MEMORY").unwrap();

	stats.create_db(&mut conn).unwrap();
	stats.write_values(&mut conn).unwrap();
	stats.create_db(&mut conn).unwrap();
	stats.write_values(&mut conn).unwrap();
    }
}
