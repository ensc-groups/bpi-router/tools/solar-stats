mod pool;

mod device_stats;

pub use pool::DbPool;

pub trait DbStats {
    fn create_db(&self, conn: &mut rusqlite::Connection) -> rusqlite::Result<()>;
    fn write_values(&self, conn: &mut rusqlite::Connection) -> rusqlite::Result<()>;
}

pub fn write_db<F>(pool: &mut DbPool, filename: F, data: &dyn DbStats, is_last: bool) -> crate::Result<()>
where
    F: AsRef<std::path::Path>
{
    let path = filename.as_ref().to_path_buf();
    let exists = path.exists();

    let conn = pool.open(path)?;

    conn.execute_batch("PRAGMA journal_mode = MEMORY")?;

    match is_last {
	false	=> conn.execute_batch("PRAGMA synchronous = OFF")?,
	true	=> conn.execute_batch("PRAGMA synchronous = ON")?,
    }

    if !exists {
	data.create_db(conn)?;
    }

    data.write_values(conn)?;

    debug!("saved into database");

    Ok(())
}
