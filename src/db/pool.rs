use std::collections::HashMap;
use std::path::PathBuf;

use rusqlite::Connection;

pub struct DbPool {
    conn: HashMap<PathBuf, Connection>,
}

impl DbPool {
    pub fn new() -> Self {
	Self {
	    conn:	HashMap::new()
	}
    }

    pub fn open(&mut self, path: PathBuf) -> rusqlite::Result<&mut Connection> {
	if !self.conn.contains_key(&path) {
	    debug!("creating sqlite db for {path:?}");

	    let conn = Connection::open(&path)?;

	    self.conn.insert(path.clone(), conn);
	}

	Ok(self.conn.get_mut(&path).unwrap())
    }
}
