use crate::{proto::{misc::Registers, be16}, Time};

use super::Stats;

// from https://github.com/StephanJoubert/home_assistant_solarman.git
// cecf7982d09a99abf28c5ebf34623b88fa120111
const REGISTER_SPEC: &[super::Spec] = &[
    spec!("Inverter ID",                                     String,      [0x0003, 0x0004, 0x0005, 0x0006, 0x0007]),
    spec!("Hardware Version",                                Version,     [0x000c]),
    spec!("DC Master Firmware Version",                      Version,     [0x000d]),
    spec!("AC Version",                                      Version,     [0x000e]),
    spec!("Rated Power",                                     Power,       [0x0010]),
    spec!("Protocol Version",                                Version,     [0x0012]),
    spec!("Start-up Self-checking Time",                     Duration,    [0x0015]),
    spec!("Update Time",                                     Time,        [0x0016, 0x0017, 0x0018]),
    spec!("Grid Voltage Upper Limit",                        Voltage,     [0x001b]),
    spec!("Grid Voltage Lower Limit",                        Voltage,     [0x001c]),
    spec!("Grid Frequency Upper Limit",                      Frequency,   [0x001d]),
    spec!("Grid Frequency Lower Limit",                      Frequency,   [0x001e]),
    spec!("Overfrequency And Load Reduction Starting Point", Frequency,   [0x0022]),
    spec!("Overfrequency And Load Reduction Percentage",     Percentage,  [0x0023]),
    spec!("Active Power Regulations",                        Percentage,  [0x0028]),
    spec!("ON-OFF Enable",                                   Flag,        [0x002b]),
    spec!("Island Protection Enable",                        Flag,        [0x002e]),
    spec!("Soft Start Enable",                               Flag,        [0x002f]),
    spec!("Overfrequency And Load-shedding Enable",          Flag,        [0x0031]),
    spec!("Power Factor Regulations",                        Raw,         [0x0032]),
    spec!("Restore Factory Settings",                        Flag,        [0x0036]),
    spec!("Running Status",                                  Status,      [0x003b]),
    spec!("Daily Production",                                Energy,      [0x003c]),
    spec!("Total Production",                                Energy,      [0x003f, 0x0040]),
    spec!("Daily Production 1",                              Energy,      [0x0041]),
    spec!("Daily Production 2",                              Energy,      [0x0042]),
    spec!("Total Production 1",                              Energy,      [0x0045, 0x0046]),
    spec!("Total Production 2",                              Energy,      [0x0047, 0x0048]),
    spec!("AC Voltage",                                      Voltage,     [0x0049]),
    spec!("Grid Current",                                    Current,     [0x004c]),
    spec!("AC Output Frequency",                             Frequency,   [0x004f]),
    spec!("Total AC Output Power",                           Power,       [0x0056, 0x0057]),
    spec!("Radiator Temperature",                            Temperature, [0x005a], scale => 0.01, offs => -10.0),
    spec!("PV1 Voltage",                                     Voltage,     [0x006d]),
    spec!("PV1 Current",                                     Current,     [0x006e]),
    spec!("PV2 Voltage",                                     Voltage,     [0x006f]),
    spec!("PV2 Current",                                     Current,     [0x0070]),
];

fn get_spec(idx: RegisterIdx) -> &'static super::Spec {
    &REGISTER_SPEC[idx as usize]
}

#[allow(non_camel_case_types, clippy::upper_case_acronyms)]
#[repr(usize)]
enum RegisterIdx {
    PROD_DAILY_ALL	= 22,
    PROD_DAILY_1	= 24,
    PROD_DAILY_2	= 25,
    PROD_TOTAL_ALL	= 23,
    PROD_TOTAL_1	= 26,
    PROD_TOTAL_2	= 27,

    AC_VOLTAGE		= 28,
    AC_FREQUENCY	= 30,
    AC_POWER		= 31,

    TEMPERATURE		= 32,

    PV1_VOLTAGE		= 33,
    PV1_CURRENT		= 34,
    PV2_VOLTAGE		= 35,
    PV2_CURRENT		= 36,

    STATUS		= 21,
}

fn convert_stats(_info: &super::DeviceInfo, regs: &Registers<be16>) -> Option<Stats> {
    Some(Stats {
	id:			"0".to_string(),
	tm:			Time::now(),
        uptime:			Default::default(),
        run_time:		Default::default(),
        num_pv:			2,
        prod_daily_all:		get_spec(RegisterIdx::PROD_DAILY_ALL).get_f32(regs)?,
        prod_daily:		array!([
	    get_spec(RegisterIdx::PROD_DAILY_1).get_f32(regs)?,
	    get_spec(RegisterIdx::PROD_DAILY_2).get_f32(regs)?,
	]),
        prod_total_all:		get_spec(RegisterIdx::PROD_TOTAL_ALL).get_f32(regs)?,
        prod_total:		array!([
	    get_spec(RegisterIdx::PROD_TOTAL_1).get_f32(regs)?,
	    get_spec(RegisterIdx::PROD_TOTAL_2).get_f32(regs)?,
	]),
        ac_voltage:		get_spec(RegisterIdx::AC_VOLTAGE).get_f32(regs)?,
        ac_frequency:		get_spec(RegisterIdx::AC_FREQUENCY).get_f32(regs)?,
        ac_output_power:	get_spec(RegisterIdx::AC_POWER).get_f32(regs)?,
        temperature:		get_spec(RegisterIdx::TEMPERATURE).get_f32(regs)?,
        voltage:		array!([
	    get_spec(RegisterIdx::PV1_VOLTAGE).get_f32(regs)?,
	    get_spec(RegisterIdx::PV2_VOLTAGE).get_f32(regs)?,
	]),
        current:		array!([
	    get_spec(RegisterIdx::PV1_CURRENT).get_f32(regs)?,
	    get_spec(RegisterIdx::PV2_CURRENT).get_f32(regs)?,
	]),
	status:			get_spec(RegisterIdx::STATUS).get_status(regs)?,
    })
}

pub const DEVICE: super::DeviceInfo = super::DeviceInfo {
    specs:	REGISTER_SPEC,
    num_pv:	2,
    to_stats:	&convert_stats,
};

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_00() {
	macro_rules! test {
	    ($idx:tt, $name:expr)	=> {
		assert_eq!(get_spec(RegisterIdx::$idx).name, $name);
	    }
	}

	test!(PROD_DAILY_ALL, "Daily Production");
	test!(PROD_DAILY_1,   "Daily Production 1");
	test!(PROD_DAILY_2,   "Daily Production 2");

	test!(PROD_TOTAL_ALL, "Total Production");
	test!(PROD_TOTAL_1,   "Total Production 1");
	test!(PROD_TOTAL_2,   "Total Production 2");

	test!(AC_VOLTAGE,     "AC Voltage");
	test!(AC_FREQUENCY,   "AC Output Frequency");
	test!(AC_POWER,       "Total AC Output Power");

	test!(TEMPERATURE,    "Radiator Temperature");

	test!(PV1_VOLTAGE,    "PV1 Voltage");
	test!(PV1_CURRENT,    "PV1 Current");
	test!(PV2_VOLTAGE,    "PV2 Voltage");
	test!(PV2_CURRENT,    "PV2 Current");

	test!(STATUS,         "Running Status");
    }
}
