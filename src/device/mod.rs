macro_rules! spec {
    ($name:expr, $class:tt, $regs:expr) => {
	spec!($name,$class,$regs, scale=>$crate::device::Class::$class.default_scale(), offs=>0.0)
    };

    ($name:expr, $class:tt, $scale:expr, $regs:expr) => {
	spec!($name,$class,$regs, scale=>$scale, offs=>0.0)
    };

    ($name:expr, $class:tt, $regs:expr, scale => $scale:expr, offs => $offs:expr) => {
	$crate::device::Spec {
	    name:	$name,
	    class:	$crate::device::Class::$class,
	    scale:	$scale,
	    registers:	& $regs,
	    offset:	$offs,
	}
    }
}

macro_rules! array {
    ([ $( $spec:expr ),* ])	 => {
	array!([ $( $spec ),* , ])
    };

    ([ $( $spec:expr ),* , ])	 => {
	{
	    fn fill_default<T: Copy + Default, const N: usize>() -> [T; N] {
		[T::default(); N]
	    }

	    let mut res = fill_default();
	    let mut i = 0;

	    $(
		i += 1;
		res[i - 1] = $spec;
	    )*

	    res
	}
    }
}

mod spec;
mod stats;

pub mod deye_2mppt;

pub use stats::Stats;
pub use spec::{ Class, Spec };

use crate::proto::{misc::Registers, be16};

pub struct DeviceInfo<'a> {
    pub specs:	&'a [Spec],
    pub num_pv:	u8,

    to_stats: &'a dyn Fn(&Self, &Registers<be16>) -> Option<stats::Stats>,
}


impl DeviceInfo<'_> {
    pub fn as_stats(&self, regs: &Registers<be16>) -> Option<stats::Stats> {
	(self.to_stats)(self, regs)
    }
}


#[cfg(test)]
mod test {
    #[test]
    fn test_00() {
	let arr: [f32;4] = array!([1.,2.]);
	assert_eq!(arr, [1., 2., 0., 0.]);

	let arr: [u32;5] = array!([1,2]);
	assert_eq!(arr, [1, 2, 0, 0, 0]);
    }
}
