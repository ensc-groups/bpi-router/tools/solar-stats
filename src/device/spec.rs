use std::ops::RangeInclusive;

use crate::proto::{misc::Registers, be16};

#[derive(Clone, Copy, Debug)]
pub enum Class {
    Voltage,
    Current,
    Energy,
    Frequency,
    Power,
    Temperature,
    String,
    Duration,
    Time,
    Percentage,
    Flag,
    Raw,
    Status,
    Version,
}

impl Class {
    pub const fn get_unit_delim(self) -> &'static str {
	match self {
	    Self::Percentage	=> "",
	    _			=> " ",
	}
    }

    pub const fn get_unit(self) -> &'static str {
	match self {
	    Self::Percentage	=> "%",
	    Self::Duration	=> "s",
	    Self::Temperature	=> "C",
	    Self::Voltage	=> "V",
	    Self::Current	=> "A",
	    Self::Energy	=> "Wh",
	    Self::Power		=> "W",
	    Self::Frequency	=> "Hz",
	    _			=> unimplemented!(),
	}
    }

    pub const fn default_scale(self) -> f32 {
	match self {
	    Self::Power		=> 0.1,
	    Self::Frequency	=> 0.01,
	    Self::Energy	=> 100.0,
	    Self::Voltage	=> 0.1,
	    Self::Current	=> 0.1,
	    Self::Temperature	=> 0.01,
	    _			=> 1.0,
	}
    }
}

pub struct Spec {
    pub name:		&'static str,
    pub class:		Class,
    pub scale:		f32,
    pub registers:	&'static [u16],
    pub offset:		f32,
}

impl Spec {
    pub fn get_range(spec: &[Self]) -> RangeInclusive<u16> {
	let mut min = u16::MAX;
	let mut max = u16::MIN;

	for s in spec {
	    min = s.registers.iter().fold(min, |acc, v| acc.min(*v));
	    max = s.registers.iter().fold(max, |acc, v| acc.max(*v));
	}

	min..=max
    }

    pub fn formatter<'a>(&'a self, vals: &'a Registers<'a, be16>) -> SpecFormatter<'a> {
	SpecFormatter(self, vals)
    }

    pub fn get_status(&self, regs: &Registers<be16>) -> Option<u16> {
	let v = self.registers.iter()
	    .map(|idx| regs.nth(*idx).unwrap().as_native());

	self.get_status_raw(v)
    }

    fn get_status_raw(&self, mut vals: impl DoubleEndedIterator<Item = u16>) -> Option<u16> {
	match (vals.next(), vals.next()) {
	    (Some(v), None)		=> Some(v),
	    _				=> None,
	}
    }

    pub fn get_f32(&self, regs: &Registers<be16>) -> Option<f32> {
	let v = self.registers.iter()
	    .map(|idx| regs.nth(*idx).unwrap().as_native());

	self.get_f32_raw(v)
    }

    fn get_f32_raw(&self, vals: impl DoubleEndedIterator<Item = u16>) -> Option<f32> {
	fn convert_f32(vals: impl DoubleEndedIterator<Item = u16>, scale: f32, offs: f32) -> f32 {
	    vals.rev()
		.fold(0_u64, |acc, v| (acc << 16) + (v as u64)) as f32 * scale
		+ offs
	}

	match self.class {
	    Class::Percentage |
	    Class::Duration |
	    Class::Temperature |
	    Class::Current |
	    Class::Energy |
	    Class::Power |
	    Class::Voltage |
	    Class::Frequency	=> Some(convert_f32(vals, self.scale, self.offset)),
	    _			=> None,
	}
    }

    #[allow(clippy::get_first)]
    pub fn format_value(&self, regs: &Registers<be16>, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
	use std::fmt::Debug;

	let mut vals = self.registers.iter()
	    .map(|idx| regs.nth(*idx).unwrap().as_native());

        match self.class {
	    Class::String	=> {
		let mut res = Vec::with_capacity(vals.len() + 2);

		#[allow(clippy::identity_op)]
		for c in vals {
		    res.push(((c >> 8) & 0xff) as u8);
		    res.push(((c >> 0) & 0xff) as u8);
		}

		match std::str::from_utf8(&res) {
		    Ok(s)	=> f.write_str(s),
		    Err(_)	=> f.write_fmt(format_args!("{:x?}", &res)),
		}
	    }

	    Class::Flag		=> {
		let orig = vals.clone();

		match (vals.next(), vals.next()) {
		    (Some(1), None)	=> f.write_str("ON"),
		    (Some(0), None)	=> f.write_str("OFF"),
		    (Some(v), None)	=> v.fmt(f),
		    _			=> orig.fmt(f),
		}
	    },

	    Class::Percentage |
	    Class::Duration |
	    Class::Temperature |
	    Class::Current |
	    Class::Energy |
	    Class::Power |
	    Class::Voltage |
	    Class::Frequency	 => {
		self.get_f32_raw(vals).unwrap().fmt(f)?;

		f.write_str(self.class.get_unit_delim())?;
		f.write_str(self.class.get_unit())
	    },

	    Class::Version	=> {
		let orig = vals.clone();

		match (vals.next(), vals.next()) {
		    (Some(mut v), None)		=> {
			let mut res = String::with_capacity(10);

			for i in 0..4 {
			    if i != 0 {
				res.push('.');
			    }

			    res.push_str(&(v % 16).to_string());

			    v /= 16;
			}

			let res: String = res.chars().rev().collect();

			res.fmt(f)
		    }
		    _				=> orig.fmt(f),
		}
	    }

	    Class::Time		=> {
		let orig = vals.clone();

		match (vals.next(), vals.next(), vals.next(), vals.next()) {
		    (Some(v0), Some(v1), Some(v2), None)	=> {
			f.write_fmt(format_args!("{}-{:02}-{:02} {:02}:{:02}:{:02}",
						 2000 + (v1 >> 8), v0 & 0xff,
						 v0 >> 8, v1 & 0xff,
						 v2 >> 8, v2 & 0xff))
		    },
		    _		=> orig.fmt(f),
		}
	    }

	    Class::Status	=> {
		let orig = vals.clone();

		match self.get_status_raw(vals) {
		    Some(v)		=> v.fmt(f),
		    _			=> orig.fmt(f),
		}
	    }

	    Class::Raw		=> {
		let vals: Vec<_> = vals.collect();

		vals.fmt(f)
	    }
	}
    }
}

pub struct SpecFormatter<'a>(pub &'a Spec, pub &'a Registers<'a, be16>);

impl std::fmt::Display for SpecFormatter<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
	self.0.format_value(self.1, f)
    }
}
