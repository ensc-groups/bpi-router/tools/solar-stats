use std::time::Duration;

use crate::proto::PayloadExtra;

const MAX_NUM_PV: usize = 4;

#[derive(Debug, Clone)]
pub struct Stats {
    pub id:			String,
    pub tm:			crate::Time,
    pub uptime:			Duration,
    pub run_time:		Duration,

    pub num_pv:			usize,
    pub status:			u16,

    pub prod_daily_all:		f32,
    pub prod_daily:		[f32; MAX_NUM_PV],

    pub prod_total_all:		f32,
    pub prod_total:		[f32; MAX_NUM_PV],

    pub ac_voltage:		f32,
    pub ac_frequency:		f32,

    pub ac_output_power:	f32,

    pub temperature:		f32,

    pub voltage:		[f32; MAX_NUM_PV],
    pub current:		[f32; MAX_NUM_PV],
}

impl Stats {
    pub fn fill_extra_info(&mut self, info: &PayloadExtra) {
	self.uptime = info.uptime();
    }

    pub fn set_id(&mut self, id: u32) {
	self.id = format!("{}", id);
    }

    pub fn is_valid(&self) -> bool {
	self.status != 0
    }
}
