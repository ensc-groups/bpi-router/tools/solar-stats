#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error(transparent)]
    Io(#[from] std::io::Error),

    #[error(transparent)]
    Nix(#[from] nix::Error),

    #[error("rrd error {:?}", .0)]
    RrdError(std::ffi::CString),

    #[error(transparent)]
    Sqlite(#[from] rusqlite::Error),

    #[error("no such host")]
    NoSuchHost,

    #[error("bad discovery response")]
    BadDiscoveryResponse,

    #[error("bad payload extra information")]
    BadPayloadExtra,
}
