use std::{mem::MaybeUninit, net::UdpSocket};

pub fn read_ptr<T: std::io::Read>(r: &mut T, ptr: * mut u8, len: usize) -> std::io::Result<()>
{
    let raw = unsafe {
	core::slice::from_raw_parts_mut(ptr, len)
    };

    r.read_exact(raw)
}

pub fn read_obj<'b, T: std::io::Read, V>(r: &mut T, v: &'b mut MaybeUninit<V>) -> std::io::Result<&'b V> {
    read_ptr(r, v as * mut _ as * mut u8, core::mem::size_of::<V>())?;

    Ok(unsafe {
	core::mem::transmute(v)
    })
}

pub fn uninit_buf<const N: usize>() -> [MaybeUninit<u8>;N] {
    [MaybeUninit::<u8>::uninit();N]
}

pub trait ReadUninit {
    fn read_uninit<'a>(&self, buf: &'a mut [MaybeUninit<u8>]) -> std::io::Result<&'a [u8]>;
}

impl ReadUninit for UdpSocket {
    fn read_uninit<'a>(&self, buf: &'a mut [MaybeUninit<u8>]) -> std::io::Result<&'a [u8]> {
        let buf: &'a mut [u8] = unsafe {
	    core::mem::transmute(buf)
	};

	let len = self.recv(buf)?;

	assert!(len <= buf.len());

	Ok(&buf[..len])
    }
}
