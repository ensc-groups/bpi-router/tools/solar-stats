#![allow(clippy::redundant_field_names)]

#[macro_use]
extern crate tracing;

use std::time::Duration;

use nix::sys::time::TimeSpec;
use nix::sys::{ timerfd, select, signalfd };
use nix::sys::signal::Signal;

use clap::Parser;

mod client;
mod error;
mod proto;
mod io;
mod device;
mod rrd;
mod db;

pub mod util;

pub use error::Error;
use sd_notify::NotifyState;
pub type Result<T> = std::result::Result<T, Error>;

pub type Time = std::time::SystemTime;

// NOTE: when changing this value, the RRA setup in rrd/netifstat.rs must be also changed
const POLL_TIMER: Duration = Duration::from_secs(120);
const POLL_TIMERSPEC: TimeSpec = TimeSpec::from_duration(POLL_TIMER);

// avoid same timestamps on restart (which will cause rrd errors) by delaying startup
const POLL_STARTSPEC: TimeSpec = TimeSpec::from_duration(Duration::from_secs(1));

#[derive(clap::ValueEnum)]
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum LogFormat {
    Default,
    Compact,
    Full,
    Json,
}

#[derive(clap::Parser, Debug)]
#[clap(author, version, about)]
struct CliOpts {
    #[clap(long, help("use systemd fd propagation"), value_parser)]
    systemd:		bool,

    #[clap(short, long, value_parser)]
    pub logger:		String,

    #[clap(short('L'), long, value_parser, value_name("FMT"), help("log format"),
	   default_value("default"))]
    log_format:		LogFormat,

    #[clap(short('S'), long, value_parser, value_name("COUNT"),
	   help("only update the RRD data every COUNT of measurement; used to reduce disk activity"),
	   default_value("3"))]
    skip_rrd:		u32,

    #[clap(short('R'), long, value_parser, help("directory for rrd files"))]
    rrddir:		std::path::PathBuf,

    #[clap(long, value_parser, help("query inverter once; ignore rrd"))]
    once:		bool,
}

fn save_single_stat<T>(rrddir: &std::path::Path, prefix: &str, stat: &T) -> Result<()>
where
    T: rrd::RrdDataSet
{
    let fname = prefix.to_owned() + stat.get_base_name() + ".rrd";
    let path = rrddir.join(fname);

    if !path.exists() {
	rrd::create(&path, stat.get_min_tm(), POLL_TIMER, stat)?;
    }

    rrd::update(&path, stat)
}

fn save_single_db<T>(pool: &mut db::DbPool, basedir: &std::path::Path, prefix: &str, stat: &T, is_last: bool) -> Result<()>
where
    T: db::DbStats + rrd::RrdDataSet
{
    let fname = prefix.to_owned() + stat.get_base_name() + ".sqlite";
    let path = basedir.join(fname);

    db::write_db(pool, &path, stat, is_last)
}

fn save_device_stats(pool: &mut db::DbPool, rrddir: &std::path::Path, stats: &mut DeviceStatsVec) {
    while let Some(s) = stats.pop_front() {
	let is_last = stats.is_empty();

	debug!("rrd -> {:?}", s);

	if let Err(e) = save_single_stat(rrddir, "solar:", &s) {
	    error!("rrd: failed to save stat: {:?}", e);
	}

	if let Err(e) = save_single_db(pool, rrddir, "solar:", &s, is_last) {
	    error!("db: failed to save stat: {:?}", e);
	}
    }
}

fn do_notify(state: NotifyState, use_systemd: bool) {
    if use_systemd {
	let _ = sd_notify::notify(false, &[state]);
    }
}

fn unblock_signal(sig: i32) -> Result<()> {
    let sigmask = {
	let mut mask = signalfd::SigSet::empty();

	mask.add(Signal::try_from(sig)?);
	mask
    };

    sigmask.thread_unblock()?;

    Ok(())
}

type DeviceStatsVec = std::collections::VecDeque<device::Stats>;
fn main() -> Result<()> {
    let mut args = CliOpts::parse();

    if args.log_format == LogFormat::Default {
	args.log_format = if args.systemd {
	    // when running under systemd, do not emit the timestamp because
	    // output is usually recorded in the journal.  Accuracy in journal
	    // should suffice for most usecases.

	    LogFormat::Compact
	} else {
	    LogFormat::Full
	}
    }

    let fmt = tracing_subscriber::fmt()
	.with_env_filter(tracing_subscriber::EnvFilter::from_default_env());

    match args.log_format {
	LogFormat::Compact		=> fmt.without_time().init(),
	LogFormat::Json			=> fmt.json().init(),
	LogFormat::Full			=> fmt.init(),
	LogFormat::Default		=> unreachable!(),
    }

    let mut client = client::Client::new(&args.logger, &device::deye_2mppt::DEVICE);

    if args.once {
	client.run()?;
	return Ok(());
    }

    let sigmask = {
	let mut mask = signalfd::SigSet::empty();
	mask.add(Signal::SIGINT);
	mask.add(Signal::SIGUSR1);
	mask.add(Signal::SIGUSR2);
	mask.add(Signal::SIGTERM);
	mask
    };

    let fd_tm = timerfd::TimerFd::new(timerfd::ClockId::CLOCK_BOOTTIME,
				      timerfd::TimerFlags::TFD_CLOEXEC |
				      timerfd::TimerFlags::TFD_NONBLOCK)?;
    let mut fd_sig = signalfd::SignalFd::with_flags(&sigmask,
						signalfd::SfdFlags::SFD_CLOEXEC |
						signalfd::SfdFlags::SFD_NONBLOCK)?;

    let rrddir = args.rrddir;

    let mut device_stats = DeviceStatsVec::new();

    do_notify(NotifyState::Ready, args.systemd);

    fd_tm.set(timerfd::Expiration::IntervalDelayed(POLL_STARTSPEC, POLL_TIMERSPEC),
	      timerfd::TimerSetTimeFlags::empty())?;

    do_notify(sd_notify::NotifyState::WatchdogUsec(POLL_TIMER.as_micros() as u32 * 2), args.systemd);
    do_notify(sd_notify::NotifyState::Watchdog, args.systemd);

    sigmask.thread_block()?;

    info!("started");

    let mut is_alive = true;
    let mut skip_rrd = None;
    let mut do_dump_regs = true;

    let mut db_pool = db::DbPool::new();

    while is_alive {
	use std::os::unix::io::AsRawFd;

	let mut fdset = select::FdSet::new();
	let mut do_query = skip_rrd.is_none(); // None marks first loop

	fdset.insert(fd_tm.as_raw_fd());
	fdset.insert(fd_sig.as_raw_fd());

	match select::select(None, &mut fdset, None, None, None) {
	    Err(nix::Error::EINTR)	=> continue,
	    v				=> v,
	}?;

	if fdset.contains(fd_sig.as_raw_fd()) {
	    match fd_sig.read_signal() {
		Ok(Some(info)) if [Signal::SIGTERM as u32,
				   Signal::SIGKILL as u32,
				   Signal::SIGINT as u32].contains(&info.ssi_signo)	=> {
		    debug!("got termination signal; quitting");
		    is_alive = false;
		    do_query = true;
		    skip_rrd = Some(0);
		    do_notify(sd_notify::NotifyState::Stopping, args.systemd);

		    let _ = unblock_signal(info.ssi_signo as i32);
		},

		Ok(Some(info)) if [Signal::SIGUSR1 as u32].contains(&info.ssi_signo)	=> {
		    info!("got SIGUSR1; dumping stats");
		    skip_rrd = Some(0);
		    do_query = true;
		    do_notify(sd_notify::NotifyState::Status("got SIGUSR1; dumping stats"),
			      args.systemd);
		}

		Ok(Some(info)) if [Signal::SIGUSR2 as u32].contains(&info.ssi_signo)	=> {
		    info!("got SIGUSR2; dumping regs");
		    do_dump_regs = true;
		    do_query = true;
		},

		_				=> {},
	    }
	}

	if fdset.contains(fd_tm.as_raw_fd()) {
	    let _ = fd_tm.wait();

	    do_query = true;

	    do_notify(sd_notify::NotifyState::Watchdog, args.systemd);
	}

	if do_query {
	    client.set_dump_regs(do_dump_regs);

	    match client.run() {
		Err(e) if client.is_offline()	=> {
		    debug!("failed to query logger: {:?}", e);
		    do_dump_regs = true;
		},
		Err(e)				=> warn!("failed to query logger: {:?}", e),
		Ok(stats) if !stats.is_valid()	=> warn!("invalid stats; skipping"),
		Ok(stats)			=> {
		    do_dump_regs = false;
		    device_stats.push_back(stats);
		}
	    }

	    match skip_rrd {
		Some(v) if v > 0	=> skip_rrd = Some(v - 1),
		_			=> { },
	    }
	}

	match skip_rrd {
	    None | Some(0)	=> {
		save_device_stats(&mut db_pool, &rrddir, &mut device_stats);

		skip_rrd = Some(args.skip_rrd);
	    },

	    _		=>	debug!("skipping rrd update"),
	}
    }

    Ok(())
}
