use std::mem::MaybeUninit;

use crate::util::HexU8;

use super::{ be16, le16, ModBus };

#[derive(Debug, Clone)]
#[repr(packed)]
#[allow(dead_code)]
pub struct Header {
    code:	Code,
    length:	le16,
    ctrl:	Ctrl,
    serial_no:	be16,
    serial:	[u8;4],
}

impl Header {
    pub const LEN: usize = core::mem::size_of::<Header>();

    pub fn as_bytes(&self) -> &[u8] {
	let raw: &[u8; Self::LEN] = unsafe {
	    core::mem::transmute(self)
	};

	raw
    }

    pub fn get_ctrl(&self) -> Ctrl {
	self.ctrl
    }
}

#[derive(Clone, Debug)]
#[repr(packed)]
struct Tail {
    crc:	u8,
    code:	Code,
}

impl Tail {
    pub const LEN: usize = core::mem::size_of::<Tail>();

    pub fn as_bytes(&self) -> &[u8] {
	let raw: &[u8; Self::LEN] = unsafe {
	    core::mem::transmute(self)
	};

	raw
    }
}

pub struct TelegramBuilder {
    serial:	[u8;4],
}

#[derive(Copy, Clone, Debug, PartialEq)]
#[repr(transparent)]
struct Code(u8);

impl Code {
    pub const START_OF_MESSAGE: Self = Self(0xa5);
    pub const END_OF_MESSAGE: Self = Self(0x15);
}

#[derive(Copy, Clone, PartialEq, Eq)]
#[repr(transparent)]
pub struct Ctrl(le16);

impl Ctrl {
    pub const INFO: Self = Self::new(0x4510);
    pub const INFO_RESPONSE: Self = Self::new(0x1510);

    #[allow(dead_code)]
    pub const KEEP_ALIVE: Self = Self::new(0x4710);

    const fn new(v: u16) -> Self {
	Self(le16::from_native(v))
    }

    pub fn modbus_position(self) -> Option<usize> {
	match self {
	    Self::INFO_RESPONSE	=> Some(14),
	    _			=> None,
	}
    }
}

impl std::fmt::Debug for Ctrl {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("Ctrl").field(&self.0.as_native().to_be_bytes().map(HexU8)).finish()
    }
}

#[derive(Debug)]
pub struct Telegram<'a> {
    pub header:		Header,
    pub payload:	&'a [u8],
    tail:		Tail,
}

impl <'a> Telegram<'a> {
    pub fn get_payload(&self) -> &'a [u8] {
	match self.header.get_ctrl().modbus_position() {
	    Some(p)	=> &self.payload[p..self.payload.len() - 2],
	    None	=> self.payload,
	}
    }

    pub fn get_payload_extra(&self) -> &'a [u8] {
	match self.header.get_ctrl().modbus_position() {
	    Some(p)	=> &self.payload[..p],
	    None	=> self.payload,
	}
    }

    #[cfg(xxx)]
    pub fn send<T: std::io::Write>(&self, w: &mut T) -> std::io::Result<()> {
	let mut data: [IoSlice;3] = [
	    IoSlice::new(self.header.as_bytes()),
	    IoSlice::new(self.payload),
	    IoSlice::new(self.tail.as_bytes()),
	];

	w.write_all_vectored(&mut data)
    }

    pub fn send<T: std::io::Write>(&self, w: &mut T) -> std::io::Result<()> {
	let mut data = Vec::with_capacity(self.header.as_bytes().len() +
				      self.payload.len() +
				      self.tail.as_bytes().len());

	data.extend_from_slice(self.header.as_bytes());
	data.extend_from_slice(self.payload);
	data.extend_from_slice(self.tail.as_bytes());

	w.write_all(&data)
    }

    pub fn recv<T: std::io::Read>(r: &mut T, buf: &'a mut [MaybeUninit<u8>]) -> std::io::Result<Self> {
	let mut hdr: MaybeUninit<Header> = MaybeUninit::uninit();

	let hdr = crate::io::read_obj(r, &mut hdr)?;

	if hdr.code != Code::START_OF_MESSAGE {
	    warn!("no start-of-message code in response");
	    return Err(std::io::Error::from(std::io::ErrorKind::InvalidInput));
	}

	let len = hdr.length;
	let len = len.as_native() as usize;

	if buf.len() < len {
	    return Err(std::io::Error::from(std::io::ErrorKind::OutOfMemory));
	};

	crate::io::read_ptr(r, buf as * mut _ as * mut u8, len)?;

	let mut tail: MaybeUninit<Tail> = MaybeUninit::uninit();
	let tail = crate::io::read_obj(r, &mut tail)?;

	if tail.code != Code::END_OF_MESSAGE {
	    warn!("invalid tail code");
	    return Err(std::io::Error::from(std::io::ErrorKind::InvalidInput));
	}

	let payload = unsafe {
	    core::slice::from_raw_parts(buf as * const _ as * const u8, len)
	};

	let crc = TelegramBuilder::calc_crc(hdr, payload);

	if tail.crc != crc {
	    warn!("crc mismatch; header={hdr:?}, tail={tail:?}");
	    return Err(std::io::Error::from(std::io::ErrorKind::InvalidInput));
	}

	match hdr.get_ctrl().modbus_position() {
	    Some(pos) if payload.len() < pos + 2		=> {
		warn!("payload too small ({}) for modbus content", payload.len());
		return Err(std::io::Error::from(std::io::ErrorKind::InvalidInput));
	    },

	    Some(pos) if !ModBus::verify(&payload[pos..], true)	=> {
		warn!("crc mismatch in modbus content");
		return Err(std::io::Error::from(std::io::ErrorKind::InvalidInput));
	    },

	    _ => {}
	}

	Ok(Self {
	    header:	hdr.clone(),
	    payload:	payload,
	    tail:	tail.clone(),
	})
    }
}

impl TelegramBuilder {
    pub fn new(serial: u32) -> Self {
	Self {
	    serial:	serial.to_le_bytes()
	}
    }

    fn calc_crc(hdr: &Header, payload: &[u8]) -> u8 {
	let raw: &[u8; Header::LEN] = unsafe {
	    core::mem::transmute(hdr)
	};

	let crc = raw[1..].iter().fold(0u8, |acc, v| acc.wrapping_add(*v));

	payload.iter().fold(crc, |acc, v| acc.wrapping_add(*v))
    }

    pub fn create_telegram<'a>(&self, ctrl: Ctrl, payload: &'a [u8]) -> Telegram<'a> {
	let header = Header {
	    code:		Code::START_OF_MESSAGE,
	    length:		(payload.len() as u16).into(),
	    ctrl:		ctrl,
	    serial_no:		0.into(),
	    serial:		self.serial,
	};

	let crc = Self::calc_crc(&header, payload);

	Telegram {
	    header:	header,
	    payload:	payload,
	    tail:	Tail {
		crc:		crc,
		code:		Code::END_OF_MESSAGE,
	    }
	}
    }
}
