use std::net::Ipv4Addr;


#[derive(Debug)]
pub struct WifiKitResponse {
    pub ip: Ipv4Addr,
    pub mac: [u8;6],
    pub serial: u32,
}

impl WifiKitResponse {
    #[cfg(xxx)]
    fn parse_ip(s: &[u8]) -> Option<Ipv4Addr> {
	Ipv4Addr::parse_ascii(s).ok()
    }

    fn parse_ip(s: &[u8]) -> Option<Ipv4Addr> {
	std::str::from_utf8(s).ok()?.parse().ok()
    }

    fn parse_serial(s: &[u8]) -> Option<u32> {
	let mut res: u32 = 0;

	for v in s {
	    res = res.checked_mul(10)?;

	    match v {
		b'0'..=b'9'	=> res = res.checked_add((v - b'0') as u32)?,
		_		=> {
		    error!("invalid character {v} in serial");
		    return None;
		}
	    }
	}

	Some(res)
    }

    fn parse_mac(s: &[u8]) -> Option<[u8;6]> {
	let mut mac: u64 = 0;

	if s.len() != 12 {
	    error!("invalid mac address {}", String::from_utf8_lossy(s));
	    return None;
	}

	for v in s {
	    mac <<= 4;

	    match v {
		b'0'..=b'9'	=> mac |= (v - b'0') as u64,
		b'a'..=b'f'	=> mac |= (v - b'a' + 10) as u64,
		b'A'..=b'F'	=> mac |= (v - b'A' + 10) as u64,
		_		=> {
		    error!("invalid character {v} in mac address");
		    return None;
		}
	    }
	}

	let mut res: [u8;6] = [0;6];

	res.copy_from_slice(&mac.to_be_bytes()[2..8]);

	Some(res)
    }

    pub fn parse(buf: &[u8]) -> Option<Self> {
	let mut ip = None;
	let mut mac = None;
	let mut serial = None;

	for (idx, v) in buf.split(|c| *c == b',').enumerate() {
	    match idx {
		0	=> ip = Some(v),
		1	=> mac = Some(v),
		2	=> serial = Some(v),
		_	=> {
		    error!("response {} contains too much fields", String::from_utf8_lossy(buf));
		    return None;
		}
	    }
	}

	Some(Self {
	    ip:		Self::parse_ip(ip?)?,
	    mac:	Self::parse_mac(mac?)?,
	    serial:	Self::parse_serial(serial?)?,
	})
    }
}
