use std::{slice::Iter, time::Duration};

use super::le32;

#[derive(Debug, Clone)]
#[repr(packed)]
#[allow(dead_code)]
pub struct PayloadExtra {
    marker:	[u8;2],		// [0x02, 0x01]
    rel_tm:	le32,
    uptime:	le32,
    ref_tm:	le32,
}

impl PayloadExtra {
    pub const LEN: usize = core::mem::size_of::<Self>();

    pub fn from_raw(buf: &[u8]) -> Option<Self> {
	if buf.len() != Self::LEN {
	    warn!("PayloadExtra: unexpected buffer len {}", buf.len());
	    return None;
	}

	if buf[0..2] != [0x02, 0x01] {
	    warn!("PayloadExtra: bad marker {:02x?}", &buf[0..2]);
	    return None;
	}

	let (head, res, tail) = unsafe {
	    buf.align_to::<Self>()
	};

	assert_eq!(head.len(), 0);
	assert_eq!(tail.len(), 0);
	assert_eq!(res.len(), 1);

	Some(res[0].clone())
    }

    pub fn total_tm(&self) -> std::time::SystemTime {
	std::time::SystemTime::UNIX_EPOCH
	    + Duration::from_secs(self.ref_tm() as u64 +
				  self.rel_tm() as u64)
    }

    pub fn ref_tm(&self) -> u32 {
	let v = self.ref_tm;

	v.as_native()
    }

    pub fn rel_tm(&self) -> u32 {
	let v = self.rel_tm;

	v.as_native()
    }

    #[allow(dead_code)]
    pub fn uptime(&self) -> Duration {
	let uptime = self.uptime;

	Duration::from_millis(uptime.as_native().into())
    }
}

#[derive(Copy, Clone)]
#[repr(packed)]
pub struct Unaligned<T: Copy>(T);

impl <T: Copy + std::fmt::Debug> std::fmt::Debug for Unaligned<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
	let v = self.0;

	v.fmt(f)
    }
}

#[derive(Debug)]
pub struct Registers<'a, T: Copy>{
    regs:	&'a [Unaligned<T>],
    start:	u16,
}

impl <'a, T: Copy> Registers<'a, T> {
    pub fn from_slice(start: u16, buf: &'a [u8]) -> Option<Self> {
	let (head, data, tail) = unsafe {
	    buf.align_to::<Unaligned<T>>()
	};

	if !head.is_empty() {
	    // this should not happen due to Unaligned<>
	    error!("alignment error in register slice");
	    return None;
	}

	if !tail.is_empty() {
	    warn!("remaining elements in register slice");
	    return None;
	}

	Some(Self{
	    regs:	data,
	    start:	start,
	})
    }

    #[allow(dead_code)]
    pub fn iter(&self) -> RegisterIterator<'a, T> {
	RegisterIterator {
	    regs: self.regs.iter(),
	}
    }

    pub fn nth(&self, idx: u16) -> Option<T> {
	let idx = idx as usize;
	let start = self.start as usize;

	if idx < start || idx - start >= self.regs.len() {
	    return None;
	}

	Some(self.regs[idx - start].0)
    }
}

pub struct RegisterIterator<'a, T: Copy> {
    regs: Iter<'a, Unaligned<T>>,
}

impl <T: Copy> Iterator for RegisterIterator<'_, T> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        self.regs.next().map(|v| v.0)
    }
}
