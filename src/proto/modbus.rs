use std::ops::RangeInclusive;

pub enum ModBus {
    ReadHoldingRegisters(RangeInclusive<u16>),
}

impl ModBus {
    const POLY: u16 = 0xa001;
    const CODE_READ: u8 = 3;

    fn crc(data: &[u8]) -> u16 {
	let mut crc: u16 = 0xffff;

	for b in data {
	    crc ^= *b as u16;

	    for _ in 0..8 {
		crc = match crc & 1 {
		    1	=> (crc >> 1) ^ Self::POLY,
		    _	=> crc >> 1
		};
	    }
	}

	crc
    }

    pub fn verify(data: &[u8], is_response: bool) -> bool {
	if data.len() < 4 {
	    warn!("modbus: packet too small");
	    return false;
	}

	#[cfg(XXX)]
	debug!("crc(data) = {:x}, crc=[{:x}, {:x}], data={data:x?}",
	       Self::crc(&data[..(data.len() - 2)]),
	       data[data.len() - 2], data[data.len() - 1]);

	if Self::crc(data) != 0 {
	    warn!("modbus: crc mismatch");
	    return false;
	}

	if is_response {
	    match data[1] {
		1|2|3|4	=> {
		    if data[2] as usize + 5 != data.len() {
			warn!("modbus: len field mismatch ({} vs {})", data[2] + 5, data.len());
			return false;
		    }
		},

		_	=> {},
	    }
	}

	true
    }

    fn new_read(slave_id: u8, range: &RangeInclusive<u16>) -> Vec<u8> {
	let mut res = Vec::with_capacity(6 + 2);

	res.push(slave_id);
	res.push(Self::CODE_READ);

	res.extend_from_slice(&range.start().to_be_bytes());
	res.extend_from_slice(&(range.clone().count() as u16).to_be_bytes());

	res
    }

    pub fn generate(&self, slave_id: u8) -> Vec<u8> {
	let mut res = match self {
	    Self::ReadHoldingRegisters(range)	=> Self::new_read(slave_id, range),
	};

	let crc = Self::crc(&res);
	res.extend_from_slice(&crc.to_le_bytes());

	res
    }
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    /// some examples from
    ///
    /// https://ipc2u.de/artikel/wissenswertes/modbus-rtu-einfach-gemacht-mit-detaillierten-beschreibungen-und-beispielen/
    fn test_00() {
	let req = ModBus::ReadHoldingRegisters(0x6b..=0x6d).generate(0x11);

	assert_eq!(req, [0x11, 0x03, 0x00, 0x6B, 0x00, 0x03, 0x76, 0x87]);
	assert!(ModBus::verify(&req, false));


	assert!(ModBus::verify(&[0x11, 0x01, 0x05, 0xcd, 0x6b, 0xb2, 0x0e, 0x1b, 0x45, 0xe6], true));
	assert!(ModBus::verify(&[0x11, 0x06, 0x00, 0x01, 0x00, 0x03, 0x9a, 0x9b], true));
    }

    #[test]
    fn test_01() {
	// too small
	assert!(!ModBus::verify(&[1,2,3], false));

	// bad crc
	assert!(!ModBus::verify(&[0xff, 0x06, 0x00, 0x01, 0x00, 0x03, 0x9a, 0x9b], true));

	// bad 'len' field
	assert!(!ModBus::verify(&[0x11, 0x01, 0xff, 0xcd, 0x6b, 0xb2, 0x0e, 0x1b, 0x51, 0xbc], true));
	assert!(ModBus::verify(&[0x11, 0x01, 0xff, 0xcd, 0x6b, 0xb2, 0x0e, 0x1b, 0x51, 0xbc], false));
    }
}
