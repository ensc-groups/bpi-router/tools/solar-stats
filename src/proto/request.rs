pub enum Request<'a> {
    SendDataField(&'a [u8]),
}

impl Request<'_> {
    const SEND_DATA_FIELD: [u8;15] = [ 0x02, 0x00, 0x00, 0x00,  0x00, 0x00, 0x00, 0x00,
				       0x00, 0x00, 0x00, 0x00,  0x00, 0x00, 0x00];

    fn new_send_data_field(data: &[u8]) -> Vec<u8> {
	let mut res = Vec::with_capacity(15 + data.len());

	res.extend_from_slice(&Self::SEND_DATA_FIELD);
	res.extend_from_slice(data);

	res
    }

    pub fn generate(&self) -> Vec<u8> {
	match self {
	    Self::SendDataField(data)	=> Self::new_send_data_field(data),
	}
    }
}
