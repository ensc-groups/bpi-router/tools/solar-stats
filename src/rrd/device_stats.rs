use super::{ RrdDataSet, RrdDataSpec, RrdValue };
use crate::device::Stats;

use super::DataType as DT;

const HEARTBEAT: u32 = 600;

static DS_2: &[RrdDataSpec] = &[
    RrdDataSpec {
	name:	"prod_total",
        data_type: DT::Counter {
	    heartbeat: HEARTBEAT,
	    min: 0,
	    max: i64::MAX,
	},
    },

    RrdDataSpec {
	name:	"prod_total_0",
        data_type: DT::Counter {
	    heartbeat: HEARTBEAT,
	    min: 0,
	    max: i64::MAX,
	},
    },

    RrdDataSpec {
	name:	"prod_total_1",
        data_type: DT::Counter {
	    heartbeat: HEARTBEAT,
	    min: 0,
	    max: i64::MAX,
	},
    },

    RrdDataSpec {
	name:	"ac_voltage",
        data_type: DT::Gauge {
	    heartbeat: HEARTBEAT,
	    min: RrdValue::Float(0.),
	    max: RrdValue::Float(300.),
	},
    },

    RrdDataSpec {
	name:	"ac_frequency",
        data_type: DT::Gauge {
	    heartbeat: HEARTBEAT,
	    min: RrdValue::Float(20.),
	    max: RrdValue::Float(80.),
	},
    },

    RrdDataSpec {
	name:	"ac_output_power",
        data_type: DT::Gauge {
	    heartbeat: HEARTBEAT,
	    min: RrdValue::Integer(0),
	    max: RrdValue::Integer(10_000),
	},
    },

    RrdDataSpec {
	name:	"temperature",
        data_type: DT::Gauge {
	    heartbeat: HEARTBEAT,
	    min: RrdValue::Float(-50.),
	    max: RrdValue::Float(100.),
	},
    },

    RrdDataSpec {
	name:	"voltage_0",
        data_type: DT::Gauge {
	    heartbeat: HEARTBEAT,
	    min: RrdValue::Float(0.),
	    max: RrdValue::Float(100.),
	},
    },

    RrdDataSpec {
	name:	"voltage_1",
        data_type: DT::Gauge {
	    heartbeat: HEARTBEAT,
	    min: RrdValue::Float(0.),
	    max: RrdValue::Float(100.),
	},
    },

    RrdDataSpec {
	name:	"current_0",
        data_type: DT::Gauge {
	    heartbeat: HEARTBEAT,
	    min: RrdValue::Float(0.),
	    max: RrdValue::Float(100.),
	},
    },

    RrdDataSpec {
	name:	"current_1",
        data_type: DT::Gauge {
	    heartbeat: HEARTBEAT,
	    min: RrdValue::Float(0.),
	    max: RrdValue::Float(100.),
	},
    },
];

// step: 2min
//
// 40320/2min  ... 56 days
// 48384/10min ... 12 months
// 40320/ 3h   ... 15 years
static RRA: &[&str] = &[
    "RRA:AVERAGE:0.5:1:40320",
    "RRA:AVERAGE:0.5:5:48384",
    "RRA:AVERAGE:0.5:90:40320",

    "RRA:MAX:0.5:1:40320",
    "RRA:MAX:0.5:5:48384",
    "RRA:MAX:0.5:90:40320",

    "RRA:LAST:0.5:1:40320",
    "RRA:LAST:0.5:5:48384",
    "RRA:LAST:0.5:90:40320",
];

impl RrdDataSet for Stats {
    fn get_base_name(&self) -> &str {
        &self.id
    }

    fn get_ds(&self)-> &[RrdDataSpec] {
	match self.num_pv {
	    2	=> DS_2,
	    _	=> unimplemented!(),
	}
    }

    fn get_rra(&self) -> &[&'static str] {
        RRA
    }

    fn get_values(&self) -> Vec<(crate::Time,Vec<RrdValue>)> {
	match self.num_pv {
	    2	=> vec![(
		self.tm, vec![
		    self.prod_total_all.into(),
		    self.prod_total[0].into(),
		    self.prod_total[1].into(),
		    self.ac_voltage.into(),
		    self.ac_frequency.into(),
		    self.ac_output_power.into(),
		    self.temperature.into(),
		    self.voltage[0].into(),
		    self.voltage[1].into(),
		    self.current[0].into(),
		    self.current[1].into(),
		])],

	    _	=> unimplemented!(),
	}
    }

    fn get_min_tm(&self) -> crate::Time {
	self.tm - std::time::Duration::from_secs(60)
    }
}
