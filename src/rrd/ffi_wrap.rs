use std::ffi::{CString, CStr};
use std::os::unix::prelude::OsStrExt;
use nix::libc;

use crate::{ Result, Error };

use super::ffi::{rrd_get_error, rrd_clear_error};

pub(super) fn try_rc(rc: nix::libc::c_int) -> Result<()> {
    if rc < 0 {
	let err = get_error();
	unsafe { rrd_clear_error() };
	Err(Error::RrdError(err))
    } else {
	Ok(())
    }
}

fn convert_filename<F>(filename: F) -> CString
where
    F: AsRef<std::path::Path>
{
    CString::new(filename.as_ref().as_os_str().as_bytes()).unwrap()
}

fn get_tm(tm: crate::Time) -> libc::time_t {
    tm.duration_since(std::time::UNIX_EPOCH).unwrap().as_secs() as libc::time_t
}

fn get_template(ds: &[super::RrdDataSpec]) -> CString {
    let template = ds.iter().map(|ds| ds.name).collect::<Vec<&str>>().join(":");

    CString::new(template).unwrap()
}

struct Argv<'a> {
    inner:	Vec<* const libc::c_char>,
    _m:		&'a core::marker::PhantomData<&'a CStr>,
}

impl <'a> Argv<'a> {
    pub fn new(argv: &'a [CString]) -> Self {
	let mut res = Vec::with_capacity(argv.len() + 1);

	for a in argv {
	    res.push(a.as_ptr() as * const libc::c_char);
	}

	res.push(core::ptr::null());

	Self {
	    inner:	res,
	    _m:		&core::marker::PhantomData,
	}
    }

    pub fn as_ptr(&self) -> * const * const libc::c_char {
	self.inner.as_ptr()
    }

    pub fn len(&self) -> libc::c_int {
	(self.inner.len() - 1) as libc::c_int
    }
}

pub fn get_error() -> CString {
    unsafe { CStr::from_ptr(rrd_get_error()) }.into()
}

pub fn create<F>(filename: F, start_tm: crate::Time,
		 step: std::time::Duration, data: &dyn super::RrdDataSet) -> Result<()>
where
    F: AsRef<std::path::Path>
{
    let filename = convert_filename(filename);

    let tm  = get_tm(start_tm);
    let ds  = data.get_ds().iter().map(|ds| ds.as_cstring());
    let rra = data.get_rra().iter().map(|rra| CString::new(*rra).unwrap());

    let mut argv = Vec::<CString>::new();

    argv.extend(ds.chain(rra));

    let argv = Argv::new(&argv);

    let rc = unsafe {
	super::ffi::rrd_create_r(
	    filename.as_ptr(),
	    step.as_secs() as libc::c_ulong,
	    tm as libc::time_t - 24 * 3600,
	    argv.len(),
	    argv.as_ptr()
	)
    };

    super::try_rc(rc)
}

pub fn update<F>(filename: F, data: &dyn super::RrdDataSet) -> Result<()>
where
    F: AsRef<std::path::Path>
{
    let filename = convert_filename(filename);
    let template = get_template(data.get_ds());

    let values = data.get_values();

    let mut argv: Vec<CString> = Vec::with_capacity(values.len());

    for (tm, vset) in values {
	let mut v = String::with_capacity(16 + vset.len() * 12);
	v.push_str(&get_tm(tm).to_string());

	for val in vset {
	    v.push(':');
	    v.push_str(&val.as_rrdstr());
	}

	argv.push(CString::new(v).unwrap());
    }

    debug!("argv={:?}", argv);

    let argv = Argv::new(&argv);

    let rc = unsafe {
	super::ffi::rrd_updatex_r(
	    filename.as_ptr(),
	    template.as_ptr(),
	    0,
	    argv.len(),
	    argv.as_ptr()
	)
    };

    super::try_rc(rc)
}
