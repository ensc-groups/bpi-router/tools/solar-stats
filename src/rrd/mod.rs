mod ffi;
mod ffi_wrap;

mod device_stats;

use std::ffi::CString;

pub use ffi_wrap::*;

pub enum RrdValue {
    Integer(i64),
    Float(f64),
}

impl RrdValue {
    pub fn as_rrdstr(&self) -> String {
	match self {
	    Self::Integer(v)	=> format!("{v}"),
	    Self::Float(v)	=> format!("{v}"),
	}
    }
}

impl From<i64> for RrdValue {
    fn from(value: i64) -> Self {
        Self::Integer(value)
    }
}

impl From<u32> for RrdValue {
    fn from(value: u32) -> Self {
        Self::Integer(value as i64)
    }
}

impl From<u64> for RrdValue {
    fn from(value: u64) -> Self {
        Self::Integer(value as i64)
    }
}

impl From<f32> for RrdValue {
    fn from(value: f32) -> Self {
        Self::Float(value as f64)
    }
}

#[allow(dead_code)]
pub enum DataType {
    Gauge    { heartbeat: u32, min: RrdValue, max: RrdValue },
    Counter  { heartbeat: u32, min: i64, max: i64 },
    Derive   { heartbeat: u32, min: i64, max: i64 },
    Absolute { heartbeat: u32, min: i64, max: i64 },
    Compute  { rpn: String },
}

impl DataType {
    const fn type_string(&self) -> &'static str {
	match self {
	    Self::Gauge{..}		=> "GAUGE",
	    Self::Counter{..}		=> "COUNTER",
	    Self::Derive{..}		=> "DERIVE",
	    Self::Absolute{..}		=> "ABSOLUTE",
	    Self::Compute{..}		=> "COMPUTE",
	}
    }

    pub fn as_ds(&self) -> String {
	let type_string = self.type_string();

	match self {
	    DataType::Gauge { heartbeat, min, max } =>
		format!("{type_string}:{heartbeat}:{}:{}",
			min.as_rrdstr(), max.as_rrdstr()),
	    DataType::Counter { heartbeat, min, max } |
	    DataType::Derive { heartbeat, min, max } |
	    DataType::Absolute { heartbeat, min, max } =>
		format!("{type_string}:{heartbeat}:{min}:{max}"),
	    DataType::Compute { rpn } =>
		format!("{type_string}:{rpn}")
	}
    }
}

pub struct RrdDataSpec {
    name:	&'static str,
    data_type:	DataType,
}

impl RrdDataSpec {
    pub fn as_cstring(&self) -> CString {
	let ds_spec = self.data_type.as_ds();
	let mut res = String::with_capacity(self.name.len() + ds_spec.len() + 4);

	res.push_str("DS:");
	res.push_str(self.name);
	res.push(':');
	res.push_str(&ds_spec);

	CString::new(res.as_bytes()).unwrap()
    }
}

pub trait RrdDataSet {
    fn get_base_name(&self) -> &str;
    fn get_ds(&self)-> &[RrdDataSpec];
    fn get_values(&self) -> Vec<(crate::Time,Vec<RrdValue>)>;
    fn get_rra(&self) -> &[&'static str];
    fn get_min_tm(&self) -> crate::Time;
}
