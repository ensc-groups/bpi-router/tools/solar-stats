pub struct HexU8(pub u8);

impl std::fmt::Debug for HexU8 {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("0x{:02x}", self.0))
    }
}
